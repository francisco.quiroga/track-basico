import logging

from datetime import datetime # para los sensores
from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.sensors.base import BaseSensorOperator

log = logging.getLogger(__name__)

class MyFirstOperator(BaseOperator):
    @apply_defaults
    def __init__(self, my_operator_param, *args, **kwargs):
        self.operator_param = my_operator_param
        super(MyFirstOperator, self).__init__(*args,**kwargs)
    
    def execute(self,context):
        log.info("Hello World!")
        log.info('operator_param: %s',self.operator_param)
        #XCOM para sacar un valor de una BDD guardado por otro operador/sensor
        task_instance = context['task_instance']
        sensors_minute = task_instance.xcom_pull('my_sensor_task',key='sensors_minute')
        log.info('Valid minute determined by sensor: %s', sensors_minute)


class MyFirstSensor(BaseSensorOperator):
    @apply_defaults
    def __init__(self, *args, **kwargs):
        super(MyFirstSensor,self).__init__(*args,**kwargs)
    
    def poke(self,context):
        current_minute=datetime.now().minute
        if(current_minute % 3 != 0):
            log.info("current minute (%s) is not divisible by 3, sensor will retry.", current_minute)
            return False
        
        log.info("Current minute (%s) is divisible by 3, sensor finishing.", current_minute)
        #XCOM para mandar info a una BDD que luego otros operadores puedan leer
        task_instance = context['task_instance']
        task_instance.xcom_push('sensors_minute', current_minute)
        return True
    
#################################################################################################
##                                    Ejemplos 2                                               ##
#################################################################################################
# SENSOR
# class ConversionRatesSensor(BaseSensorOperator):
#     def __init__(self,*args,**kwargs):
#         super(ConversionRatesSensor,self).__init__(*args,**kwargs)
    
#     def poke(self,context):
#         print ('poking {}').__str__()
#         # Los pokes deben devolver booleanos
#         return check_conversion_rates_api_for_valid_data(context)


# class MyFirstPlugin(AirflowPlugin):
#     name = "my_first_plugin"
#     operators = [MyFirstOperator, MyFirstSensor] #los operadores que se exportan

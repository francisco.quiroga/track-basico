from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator

with DAG(
    dag_id='raspberry_monitor_operator',
    schedule_interval='* * * * *',
    start_date = datetime(2021,11,29),
    catchup=False,
    dagrun_timeout=timedelta(minutes=10),
    tags=['raspi','monitor','rasputin'],
) as dag:
    check_db = BashOperator(task_id='check_db',bash_command='ansible-playbook ~/Documents/track-basico/ejercicio_final/playbooks/confirm_db.yml')#DummyOperator (task_id='check_db_task')
    ansible_monitor = BashOperator(task_id='monitor_operator', bash_command='ansible-playbook ~/Documents/track-basico/ejercicio_final/playbooks/retrieve_data.yml')
    log2db=BashOperator(task_id='log_to_db', bash_command='ansible-playbook ~/Documents/track-basico/ejercicio_final/playbooks/log2db.yml')
    check_thresholds=DummyOperator(task_id='check_thresholds')
    alert_if_needed=DummyOperator(task_id='alert_if_needed')
#   log_to_db = BashOperator(task_id='log_to_db', bash_command='ansible-playbook ~/Documents/cursoIntroductorio_iquall/playbooks/log2db.yml')
ansible_monitor
check_db
check_db >> log2db
ansible_monitor >> log2db
ansible_monitor >> check_thresholds
check_thresholds >> alert_if_needed
#check_db >> ansible_monitor

import vendor_function
import time
hosts = {
    "192.168.0.4" : {"mac":"00:02:17:43:65:26"},
    "192.168.0.5" : {"mac":"14:5f:94:32:89:47"},
    "192.168.0.6" : {"mac":"00:14:F6:45:34:54"}
}

for host in hosts.items(): #Para cada elemento del diccionario
    vendor = vendor_function.get_vendor(host[1]["mac"]) #envío la mac a la función get_vendor
    if(type(vendor)==str): #si no me devuelve un error
        host[1].update({"vendor":vendor}) # Agrego el vendor
        print(host)
    else:
        print(f"ERROR {vendor}: while trying to obtain vendor for {host}")
    time.sleep(1)

import requests
def get_vendor(mac_address):
    url="https://api.macvendors.com/"+mac_address
    r = requests.get(url)
    if(r.status_code == 200):
        return r.text
    else:
        return r.status_code


# macs = ["00:02:17:43:65:26", "24:4b:fe:8e:f1:97"]
# for mac in macs:
#     vendor = get_vendor(mac)
#     print(f"MAC:{mac}\nVendor:{vendor}\n\n")
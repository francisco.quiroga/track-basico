ip = "192.168.1048.1"
bytes_list = ip.split('.')

if(len(bytes_list)==4):
    for byte in bytes_list:
        if(int(byte) < 256):print(byte)
        else: print("Error, byte cannot be greater than 255")
else:
    print("Error, there must be 4 bytes in an IP address")
